import Dependencies._

ThisBuild / scalaVersion     := "2.12.12"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "au.com.latitudefinancial"
ThisBuild / organizationName := "latitude"

lazy val root = (project in file("."))
  .settings(
    name := "latf",
    test in assembly := {},
    assemblyJarName in assembly := "runner.jar",
    libraryDependencies ++= spark ++ aws ++ snowflake ++ streaming ++ Seq(
      ammonite,
      scalaTest,
      sttpClient),
    sourceGenerators in Test += Def.task {
      val file = (sourceManaged in Test).value / "amm.scala"
      IO.write(file, """object amm extends App { ammonite.Main.main(args) }""")
      Seq(file)
    }.taskValue
  )
