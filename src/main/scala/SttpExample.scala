import sttp.client3._

object SttpExample extends App {

  val backend = getBackendWithSslDisabled

  val client = SimpleHttpClient().withBackend(backend)
  val response = client.send(basicRequest.get(uri"https://httpbin.org/get"))

  val errorCode: Int = -1
  val errorMsg = "Service not responding"
  val responseBody: String = response.body.getOrElse(errorMsg)
  val fileNameAndContent: Array[String] = responseBody.split(" ")
  os.write(os.pwd / fileNameAndContent(0), fileNameAndContent(1))

  def getBackendWithSslDisabled = {
    // https://sttp.softwaremill.com/en/latest/conf/ssl.html
    import java.security.SecureRandom
    import java.security.cert.X509Certificate
    import javax.net.ssl._

    val TrustAllCerts: X509TrustManager = new X509TrustManager() {
      def getAcceptedIssuers: Array[X509Certificate] = Array[X509Certificate]()

      override def checkServerTrusted(x509Certificates: Array[X509Certificate], s: String): Unit = ()

      override def checkClientTrusted(x509Certificates: Array[X509Certificate], s: String): Unit = ()
    }

    //  val ks: KeyStore = KeyStore.getInstance(KeyStore.getDefaultType)
    //  ks.load(new FileInputStream("/path/to/your_cert.p12"), "password".toCharArray)

    //  val kmf: KeyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm)
    //  kmf.init(ks, "password".toCharArray)

    val ssl: SSLContext = SSLContext.getInstance("TLS")
    //  ssl.init(kmf.getKeyManagers, Array(TrustAllCerts), new SecureRandom)
    ssl.init(null, Array(TrustAllCerts), new SecureRandom)

    import java.net.HttpURLConnection
    import javax.net.ssl.HttpsURLConnection

    def useSSL(conn: HttpURLConnection): Unit =
      conn match {
        case https: HttpsURLConnection => https.setSSLSocketFactory(ssl.getSocketFactory)
        case _ => ()
      }

    val backend = HttpURLConnectionBackend(customizeConnection = useSSL)
    backend
  }
}
