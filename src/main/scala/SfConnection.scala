import os.Path
import scalikejdbc._

object SfConnection {
  val cwd: Path = os.home / "lab" / "scala-playground" / "neophytes-guide-22" / ".scripts"
  val tokenPath: Path = cwd / "sftoken.txt"

  val token: String = os.read(tokenPath)

  val ROLE = "CNP-ACCESS-PLATFORM-DATA-C360-SUPPORT"
  val WAREHOUSE = "CUSTOMER360_SUPPORT_WH"
  val ACCOUNT = "customer360prod.ap-southeast-2"
  val USER = "700005171@latitudefs.com"

  val oauth = s"oauth&token=$token"
  val webauth = "externalbrowser"

  // initialize JDBC driver & connection pool
  //  Class.forName("net.snowflake.client.jdbc.SnowflakeDriver")

  def main(args: Array[String]): Unit = {
    ConnectionPool.singleton(s"jdbc:snowflake://$ACCOUNT.snowflakecomputing.com:443?role=$ROLE&warehouse=$WAREHOUSE&Authenticator=$webauth", USER, "na")
    implicit val session = AutoSession
    val res = sql"select CUSTOMER_MASTER_ID from WS_CUSTOMER360_SUPPORT.TEALIUM.AGED_CUSTOMERS limit 10".map(_.string("CUSTOMER_MASTER_ID")).list.apply()
    res.foreach(println)
  }
}
