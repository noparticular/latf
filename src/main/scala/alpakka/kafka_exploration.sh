#look available metadata for kafka instance
sudo apt-get install -y kafkacat
docker run -it --network=host edenhill/kafkacat:1.6.0 -b localhost:9092 -L
kafkacat -b localhost:9092 -L
kafkacat -b localhost:9092 -t "nuke-customer" -L

kafkacat -b kafka:9092 -t "nuke-customer" -C
kafkacat -b kafka:9092  -t "nuke-customer" \
                -K: \
                -P <<EOF
{"customer_master_id":null,"clv_customer_id":"14823174"}
EOF

./kafka-topics.sh --bootstrap-server kafka:9092 --list --topic "nuke-customer"
./kafka-console-consumer.sh --bootstrap-server kafka:9092 --topic "nuke-customer"
echo '{"customer_master_id":null,"clv_customer_id":"43842"}' | ./kafka-console-producer.sh --bootstrap-server kafka:9092 --topic "nuke-customer"


#payload = {
#  "customer_master_id" : null,
#  "clv_customer_id" : "43840"
#}
#topicName = nuke-customer

#arn:aws:iam::554672159116:role/federation/data-breakglass
#connectivity check
nc -vz b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com 9094
nc -vz localhost 9092
nc -vz localhost 4848

#./kafka-topics.sh --bootstrap-server localhost:9092 --list --topic nuke-customer --command-config client.mjn
#./kafka-topics.sh --bootstrap-server b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094 --list --topic nuke-customer --command-config client.properties

docker-compose run --rm dev bash
sbt "project nuker" "test:testOnly *SendLocalNukeRequestTest"
# ssm /latitude/cnp/kafka/nonprod/brokers
#b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094,
#b-2.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094,

#kafka_bootstrap_servers=clvib_infra_config.get("kafka_bootstrap_servers"),
#kafka_security_protocol=clvib_infra_config.get("kafka_security_protocol", "SSL"),
#topic_name=clvib_infra_config.get("kafka_topic_name", "customer-master-source"),

## Kafka
#KafkaSchemaRegistryUrl=/latitude/cnp/kafka/nonprod/schema-registry
curl -X GET http://SC-10-Servi-1PS8K0K8GPW7J-8c2e42543809dd96.elb.ap-southeast-2.amazonaws.com:8081/subjects
sudo docker run -it wurstmeister/kafka:2.13-2.6.0 bash
cd /opt/kafka/bin
echo security.protocol=SSL > client.properties
export KAFKA_HEAP_OPTS="-Xmx1G"

nc -vz b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com 9094
./kafka-topics.sh --bootstrap-server b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094 --list --command-config client.properties
./kafka-topics.sh --bootstrap-server b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094 --list --topic "nuke-customer" --command-config client.properties

#echo '{"customer_master_id":null,"clv_customer_id":"14823174"}' | ./kafka-console-producer.sh --bootstrap-server b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094 --topic "nuke-customer"
#./kafka-console-consumer.sh --bootstrap-server b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094 --topic "nuke-customer" --from-beginning

#https://docs.confluent.io/platform/current/tutorials/examples/clients/docs/kafkacat.html
./kafka-topics.sh --bootstrap-server b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094 --create --topic "nuke-customer" --partitions 3 --command-config client.properties

sudo docker run -i --network=host edenhill/kafkacat:1.6.0 -b b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094 -X security.protocol=SSL -t "nuke-customer" \
                -K: \
                -P <<EOF

1:{"customer_master_id":null,"clv_customer_id":"14823174"}
EOF

sudo docker run -it --network=host edenhill/kafkacat:1.6.0 -b b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094  -X security.protocol=SSL -L
sudo docker run -it --network=host edenhill/kafkacat:1.6.0 -b b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094  -X security.protocol=SSL -t "nuke-customer" -C
sudo docker run -it --network=host edenhill/kafkacat:1.6.0 -b b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094  -X security.protocol=SSL -t "nuke-customer-broadcast" -C.
#{"customer_master_id":"feb69cb4-f994-e905-9456-f5787b3f5889","clv_customer_id":null}

sudo docker run -it --network=host edenhill/kafkacat:1.6.0 -L -b b-1.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094,b-2.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094,b-3.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094,b-4.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094,b-5.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094,b-6.nonprod.bmvwtb.c2.kafka.ap-southeast-2.amazonaws.com:9094  -X security.protocol=SSL

#/latitude/data/customer-master/nonprod/neptune/cluster/endpoint
#/latitude/data/clv-replica/elasticsearch/domain-endpoint

## Neptune
nc -vz customer-master-nonprod-cluster.cluster-cy0f3l7qgutu.ap-southeast-2.neptune.amazonaws.com 8182
sudo docker pull tinkerpop/gremlin-console
cd /tmp
touch neptune-remote.yaml

```
hosts: [customer-master-nonprod-cluster.cluster-cy0f3l7qgutu.ap-southeast-2.neptune.amazonaws.com]
port: 8182
connectionPool: { enableSsl: true}
serializer: { className: org.apache.tinkerpop.gremlin.driver.ser.GryoMessageSerializerV3d0, config: { serializeResultToString: true }}
```
sudo docker run -v /tmp:/tmp -it tinkerpop/gremlin-console

gremlin> :remote connect tinkerpop.server /tmp/neptune-remote.yaml
gremlin> :remote console
gremlin> g.V().has('clvId', '14823174').in().valueMap()


## Elastic Search
https://ap-southeast-2.console.aws.amazon.com/ec2/v2/home?region=ap-southeast-2#ConnectToInstance:instanceId=i-08c78091d5782d8d3
sudo docker pull lolhens/ammonite:2.3.8
sudo docker run -it --network=host lolhens/ammonite:2.3.8 bash
amm <- test.sc

curl -H "Content-Type: application/json" \
  -X POST \
  -d '{"query": {"simple_query_string": {"fields": [ "clv_customer_id" ], "query": "472856402"}}}' \
  https://vpc-clv-replica-nonprod-bf6jwkqhepksrmo2wtnycndjwa.ap-southeast-2.es.amazonaws.com:443/clv/_search

curl -H "Content-Type: application/json" \
  -X POST \
  -d '{"query": {"simple_query_string": {"fields": [ "clv_customer_id" ], "query": "480277133"}}}' \
  https://vpc-clv-replica-nonprod-bf6jwkqhepksrmo2wtnycndjwa.ap-southeast-2.es.amazonaws.com:443/clv/_search



./kafka-configs.sh --alter --add-config retention.ms=604800000 --bootstrap-server=b-1.prod.eg6yfk.c4.kafka.ap-southeast-2.amazonaws.com:9094 --topic tealium-prospect-prod --command-config ssl-config
./kafka-configs.sh --alter --add-config retention.ms=604800000 --bootstrap-server=b-1.prod.eg6yfk.c4.kafka.ap-southeast-2.amazonaws.com:9094 --topic tealium-prod --command-config ssl-config


./kafka-configs.sh --describe --bootstrap-server=b-1.prod.eg6yfk.c4.kafka.ap-southeast-2.amazonaws.com:9094 --topic tealium-prospect-prod --command-config ssl-config
./kafka-configs.sh --describe --bootstrap-server=b-1.prod.eg6yfk.c4.kafka.ap-southeast-2.amazonaws.com:9094 --topic tealium-prod --command-config ssl-config
./kafka-configs.sh --describe --bootstrap-server=b-1.prod.eg6yfk.c4.kafka.ap-southeast-2.amazonaws.com:9094 --topic tealium-prod --command-config ssl-config

./kafka-consumer-groups.sh --describe --group kafka-to-responsys-prod  --bootstrap-server=b-1.prod.eg6yfk.c4.kafka.ap-southeast-2.amazonaws.com:9094  --command-config ssl-config