package au.com.latitudefinancial

import au.com.latitudefinancial.utils.DataEncryption
import org.apache.spark.sql.{SaveMode, SparkSession}

object EtlMain extends App with EtlApp {

  private implicit val spark: SparkSession = SparkSession.builder.appName("Main System Runner").master("local[*]").getOrCreate()

  val mnt = "src/test/resources"
  val out = "src/main/resources"
  val piiColumns = List("first_name", "last_name", "address")

  val rawData = spark.read.option("header", true).option("inferSchema", true).csv(s"$mnt/generated.csv")
  val processedData = anonymizeDataFrame(rawData, piiColumns, DataEncryption.randomIV())

  processedData.write.mode(SaveMode.Overwrite).option("header", value = true).option("compression","gzip").csv(s"$out/processed.csv")
  spark.close()
}
