package au.com.latitudefinancial

import au.com.latitudefinancial.utils.DataEncryption.{aesEncrypt, randomIV}
import org.apache.spark.sql.{Column, DataFrame}
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{col, lit, udf}

import scala.collection.immutable

trait EtlApp {

  def aesEncryptUdf: UserDefinedFunction = udf[String, String, String, String](aesEncrypt)

  def randomIVUdf: UserDefinedFunction = udf[String](randomIV)

  def anonymizeDataFrame(inputDf: DataFrame, anonymizedColumns: List[String], key: String)
  : DataFrame = {
    if (anonymizedColumns.isEmpty) {
      inputDf
    } else {
      val baseColumns: Array[Column] = (inputDf.columns diff anonymizedColumns).map(col)
      val encryptedColumns: immutable.Seq[Column] = anonymizedColumns.map(c => aesEncryptUdf(col(c), lit(key), randomIVUdf()).as(c))
      inputDf.select(baseColumns ++ encryptedColumns: _ *)
    }
  }
}
