package au.com.latitudefinancial

import com.amazonaws.auth.profile.ProfilesConfigFile
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.hive.thriftserver.HiveThriftServer2
import org.apache.spark.sql.functions._
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.api.java.{UDF1, UDF2}
import org.apache.spark.sql.functions.{col, explode, from_json, udf}
import org.apache.spark.sql.types.{ArrayType, IntegerType, StringType, StructField, StructType}

object Main {

  val lcl = ".scripts"
  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "prglass")

  def main(args: Array[String]): Unit = {
    val spark: SparkSession = SparkSession.builder()
      .appName("jdbc_spark_viewer")
      .master("local[*]")
      .config("spark.driver.memory","2g").config("spark.executor.memory","2g")
      .config("spark.sql.hive.thriftServer.singleSession","true") //to see temp tables through jdbc
      .config("spark.ui.enabled", "true")
      .getOrCreate()

    spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

    println("\n\n\n/* ********************* Start Spark Job*********************************************** */")
    val sqlContext = spark.sqlContext

    HiveThriftServer2.startWithContext(sqlContext)  //jdbc:hive2://localhost:10000

    def getName = udf((path: String) => path.split('/').last)

    spark.udf.register("getName", getName)
    val data = spark.read.json(s"$lcl/tapi/emails/200").withColumn("fname", input_file_name()).withColumn("tname", getName(col("fname"))).cache()
    data.createOrReplaceTempView("mail_profile")
    val cmids = spark.read.option("header", value = true).option("inferSchema", true).csv(s"$lcl/cmids.csv").cache()
    cmids.createOrReplaceTempView("cmids")
    val mails700 = spark.read.option("header", value = true).option("inferSchema", true).csv(s"$lcl/mails700.csv").cache()
    cmids.createOrReplaceTempView("mails700")


    println("/* --------------------- End Spark Job ------------------------------------------------- */\n\n\n")
//    queryTealiumJson(spark)

    while (true) {   //keep it alive for jdbc conn
      Thread.`yield`()
    }
    //    spark.stop()
  }

// def proper = {
//   val executeRestApiUDF = udf(new UDF2[String, String, String] {
//     override def call(url: String) = {
//       ExecuteVisitorGet(url, "token").getOrElse("")
//     }
//   }, StringType)
// }

  def getToken = {
    val authUrl = "https://api.tealiumiq.com/v2/auth"
    val user = "maxim.razumov@latitudefinancial.com"
    val apiKey = "~Rk~%~f[itCm32zs2{)#0,XOuFt*t]t#StDE2$%BixBe!n=2c~4naT$Dg81Z7]TE"

    val r = requests.post(authUrl, params = Map("username" -> user, "key" -> apiKey), verifySslCerts = false)
    val json = ujson.read(r.text)
    json("token").strOpt
  }

  def ExecuteVisitorGet(cmid: String, token: String) : Option[String] = {
    import requests._
    val account = "latitudefs"
    val profile = "production"

    def getVisitorByCmId(aValue: String, token: String) = {
      val attributeId = "5071"
      val attributeValue = aValue
      val vUrl = s"https://api.tealiumiq.com/v2/visitor/accounts/${account}/profiles/${profile}?attributeId=${attributeId}&attributeValue=${attributeValue}&prettyName=true"
      requests.get(vUrl, auth = RequestAuth.Bearer(token), verifySslCerts = false, check = false)
    }

    val r = getVisitorByCmId(cmid, token)
    r.statusCode match {
      case 200 => Some(r.text)
      case 404 => Some("""{"message": "404"}""")
      case _ => None
    }
  }

  private def joinCompromisedIdsWithSource700(spark: SparkSession): Unit = {
    def getName = udf((path: String) => path.split('/').last.split('.').init.last)
    spark.udf.register("getName", getName)
    val data = spark.read.json(s"$lcl/tapi/compromised").withColumn("fname", input_file_name()).withColumn("tname", getName(col("fname"))).cache()
    data.createOrReplaceTempView("peek")
    val cmids = spark.read.option("header", value = true).option("inferSchema", true).csv(s"$lcl/cmids.csv").cache()
    cmids.createOrReplaceTempView("cmids")
    //select tname, visitor.property_sets.`Customer Master IDs List [sos]` from peek inner join cmids on array_contains(peek.visitor.property_sets.`Customer Master IDs List [sos]`, cmids.CUSTOMER_MASTER_ID);
    spark.sql("""select tname, visitor.property_sets.`Customer Master IDs List [sos]` from peek inner join cmids on array_contains(peek.visitor.property_sets.`Customer Master IDs List [sos]`, cmids.CUSTOMER_MASTER_ID)""").show(false)

  }

  private def queryTealiumJson(spark: SparkSession): Unit = {
    val mnt = "s3a://lfsc360-prod-tealium-eventstream-raw/ingest_tealium_eventstream/tealium_visitor"
    val rpath = s"$mnt/year=2022/month=08/day=26/*/"
    val cmids = spark.read.option("header", value = true).option("inferSchema", true).csv(s"$lcl/cmids.csv")
    cmids.createOrReplaceTempView("cmids")
    val tjson = spark.read.json(rpath).cache()
    tjson.createOrReplaceTempView("vstream")
    spark.sql("""CACHE TABLE vcache SELECT * FROM vstream""").show(false)
  }
}

/*
jdbc:hive2://localhost:10000

thrift driver setup
org.apache.hive.jdbc.HiveDriver
/spark-2.3.0-bin-hadoop2.7/jars/commons-logging-1.1.3.jar
/spark-2.3.0-bin-hadoop2.7/jars/hadoop-common-2.7.3.jar
/spark-2.3.0-bin-hadoop2.7/jars/hive-exec-1.2.1.spark2.jar
/spark-2.3.0-bin-hadoop2.7/jars/hive-jdbc-1.2.1.spark2.jar
/spark-2.3.0-bin-hadoop2.7/jars/hive-metastore-1.2.1.spark2.jar
/spark-2.3.0-bin-hadoop2.7/jars/httpclient-4.5.4.jar
/spark-2.3.0-bin-hadoop2.7/jars/httpcore-4.4.8.jar
/spark-2.3.0-bin-hadoop2.7/jars/libthrift-0.9.3.jar
/spark-2.3.0-bin-hadoop2.7/jars/slf4j-api-1.7.16.jar
/spark-2.3.0-bin-hadoop2.7/jars/spark-hive-thriftserver_2.11-2.3.0.jar
/spark-2.3.0-bin-hadoop2.7/jars/spark-network-common_2.11-2.3.0.jar
 */

//thrift simple
//http://central.maven.org/maven2/org/apache/hadoop/hadoop-common/2.7.3/hadoop-common-2.7.3.jar
//http://central.maven.org/maven2/org/apache/hive/hive-jdbc/1.2.1/hive-jdbc-1.2.1-standalone.jar