package au.com.latitudefinancial.utils

import java.sql.Date
import java.time.temporal.ChronoUnit.DAYS

import scala.util.Random

object DataGeneration {

  val shortestNameLength = 1
  val shortestAddressLength = 12

  def generateName(seed: Double, maxLength: Int): String = {
    val (r: Random, len: Int) = init(seed, shortestNameLength, maxLength)
    r.nextString(len)
  }

  def generateId(seed: Double): Long = {
    val r = initRandomWithDouble(seed)
    Math.abs(r.nextLong)
  }

  def generateAddress(sparkSeed: Double, maxLength: Int): String = {
    val (r: Random, len: Int) = init(sparkSeed, shortestAddressLength, maxLength)
    List.fill(len)(r.nextPrintableChar).mkString
  }

  private[latitudefinancial] def init(seed: Double, minLength: Int, maxLength: Int) = {
    val r = initRandomWithDouble(seed)

    val length = Math.abs(seed * (maxLength - minLength)) + minLength
    (r, length.toInt)
  }

  def generateDate(seed: Double, minDate: Date, maxDate: Date): Date = {
    val r = initRandomWithDouble(seed)
    val localMinDate = minDate.toLocalDate
    val range = DAYS.between(localMinDate, maxDate.toLocalDate)
    val delta = r.nextInt(range.toInt)
    val res = localMinDate.plusDays(delta)
    Date.valueOf(res)
  }

  private def initRandomWithDouble(seed: Double) = {
    new Random((Math.abs(seed) * 1000).toLong)
  }
}
