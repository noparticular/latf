package au.com.latitudefinancial.utils

import java.nio.charset.StandardCharsets
import java.security.SecureRandom
import java.util.Base64

import javax.crypto.Cipher
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}

object DataEncryption {

  val encryptionAlgorithm: String = "AES/CBC/PKCS5Padding"
  val secretKeyAlgorithm: String = "AES"

  val ivAlgorithm: String = "SHA1PRNG"
  val ivSize: Int = 16

  def aesEncrypt(input: String, key: String, iv: String): String = if (input == null) {
    input
  } else {
    val secretKey: SecretKeySpec = new SecretKeySpec(decodeBase64(key), secretKeyAlgorithm)
    val randomSalt: IvParameterSpec = new IvParameterSpec(decodeBase64(iv))
    val cipher: Cipher = Cipher.getInstance(encryptionAlgorithm)
    cipher.init(Cipher.ENCRYPT_MODE, secretKey, randomSalt)
    val encrypted: Array[Byte] = cipher.doFinal(getUTF8Bytes(input))
    encodeBase64(encrypted)
  }

  def randomIV(): String = {
    val secureRandom = SecureRandom.getInstance(ivAlgorithm)
    val iv = new Array[Byte](ivSize)
    secureRandom.nextBytes(iv)
    encodeBase64(iv)
  }
  private def getUTF8Bytes(input: String): Array[Byte] = input.getBytes(StandardCharsets.UTF_8)

  private def encodeBase64(in: Array[Byte]): String = {
    Base64.getEncoder.encodeToString(in)
  }

  private def decodeBase64(in: String): Array[Byte] = {
    Base64.getDecoder.decode(in)
  }
}
