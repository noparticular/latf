import au.com.latitudefinancial.utils.SparkContextContainer
import org.scalatest.FunSuite
import org.apache.spark.sql.functions._

class ClassificationMetaTest extends FunSuite with SparkContextContainer {
  import spark.implicits._

  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  test("read meta-data") {
    val mnt = "s3a://lfsc360-dev-exadata-au-svc-repuser-raw"
    val lcl = "src/test/resources"

    val meta = spark.read.json(s"$mnt/ingest_exadata_au_svc_repuser/pii_table/columns-classification/cache.jsonl")
    val cols: Array[String] = meta.columns

   val res = meta.select(
     explode(array(cols.map(cname => struct(lit(cname).alias("key"), col(cname).alias("val"))): _*))
   ).select(
     $"col.key".as("ColumnName"),
     $"col.val.Entity".as("Entity"),
     $"col.val.Score".as("Score")
   )

    meta.printSchema()
    meta.show(false)

    res.printSchema()
    res.show(false)
  }
}
