import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import org.apache.spark.sql.SaveMode
import org.scalatest.FunSuite
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType

class ParquetMetaLoader extends FunSuite with SparkContextContainer {

  import spark.implicits._

  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "glass")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  test("load meta data") {
    val mnt = "s3a://lfsc360-dev-exadata-au-svc-repuser-raw"
    val lcl = "target/out"

    val ddl_str = "`data_source_name` STRING,`table_name` STRING,`column_name` STRING,`classification` STRING,`score` DOUBLE,`classification_date` TIMESTAMP,`manual_classification` STRING,`hash_column` BOOLEAN,`column_criteria` STRING"

    val ddlSchema = StructType.fromDDL(ddl_str)
    ddlSchema.printTreeString()

    val rs = spark.read.schema(ddlSchema).parquet(s"$lcl/asr")

    println(rs.schema.json)

    rs.printSchema()
    rs.select(col("column_name"), col("classification"), col("score")).show(50, false)
  }

  test("check sample size") {
    val mnt = "s3a://lfsc360-dev-exadata-au-svc-repuser-raw"
    val rs = spark.read.parquet(s"$mnt/pii-identification/sample-data/ingest_exadata_au_svc_repuser/pii_table/columns/LEGALADDRESS_SUBURB.parquet")


    println(s"rs.count() = ${rs.count()}")
    rs.createOrReplaceTempView("LEGALADDRESS_SUBURB")
    println(s"done")
  }

  def getName = udf((path: String) => path.split('/').init.last)

  test("distinct co") {
    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
    val rs = spark.read.json(s"$mnt/pii-identification/metadata/ingest_exadata_au_svc_repuser/*/classification.jsonl").withColumn("tname", input_file_name())
    rs.select(col("tname")).show(false)
  }

  test("summarize classification") {
    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
    val rs = spark.read.json(s"$mnt/pii-identification/metadata/ingest_exadata_au_svc_repuser/*/classification.jsonl").withColumn("tname", input_file_name())
    rs.select(col("tname")).show(false)
    rs.show(false)
    val cols: Array[String] = rs.drop("tname").columns
    val exp = rs.select($"tname",
      explode(array(cols.map(cname => struct(lit(cname).alias("key"), col(cname).alias("val"))): _*))
    ).select(
      getName($"tname").as("table_name"),
      $"col.key".as("column_name"),
      $"col.val.Entity".as("classification"),
      $"col.val.Score".as("score")
    ).where($"classification".isNotNull.and($"score".isNotNull))

    exp.createOrReplaceTempView("clsf")
    spark.sql("select * from clsf")

    exp.coalesce(1).write.mode(SaveMode.Overwrite).parquet(s"$mnt/pii-identification/summary6")
    println(s"done")
  }

  test("compare summaries") {
    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
    val olds = spark.read.parquet(s"$mnt/ingest_exadata_au_svc_repuser/columns-classification/pii_column_classification.parquet")
    val news = spark.read.parquet(s"$mnt/pii-identification/summary6")

    println(s"olds.count() = ${olds.count()}") //5279
    println(s"news.count() = ${news.count()}") //3674..,,4151

    olds.createOrReplaceTempView("olds")
    news.createOrReplaceTempView("news")

    val cmp = spark.sql(
      """select *
                          from (select ol.table_name, ol.column_name,
                                       ol.classification as oclassification,
                                       ol.score          as oscore,
                                       nw.classification as nclassification,
                                       nw.score          as nscore
                                from olds as ol
                                         left join news as nw on ol.table_name = nw.table_name and ol.column_name = nw.column_name) as base
                             """) //where nclassification <> oclassification or oscore <> nscore or nscore is null 30, 201, 16**

    //         where nscore is null group by oclassification
    cmp.coalesce(1).write.mode(SaveMode.Overwrite).option("header", true).csv(s"$mnt/pii-identification/cmp_results6")
    println("done")
  }

  test("compare csvs") {
    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
    val r1 = spark.read.option("header", true).csv(s"$mnt/pii-identification/cmp_results")
    val r2 = spark.read.option("header", true).csv(s"$mnt/pii-identification/cmp_results2")
    val r3 = spark.read.option("header", true).csv(s"$mnt/pii-identification/cmp_results3")
    val r4 = spark.read.option("header", true).csv(s"$mnt/pii-identification/cmp_results4")
    val r5 = spark.read.option("header", true).csv(s"$mnt/pii-identification/cmp_results5")

    r1.createOrReplaceTempView("r1")
    r2.createOrReplaceTempView("r2")
    r3.createOrReplaceTempView("r3")
    r4.createOrReplaceTempView("r4")
    r4.createOrReplaceTempView("r5")

    //    spark.sql("select * from r1")
    val res = spark.sql(
      """select r1.table_name, r1.column_name,
                       r1.oclassification as uatcls,
                       r1.oscore as uatscore,
                       r1.nclassification as r1cls,
                       r1.nscore          as r1score,
                       r2.nclassification as r2cls,
                       r2.nscore          as r2score,
                       r3.nclassification as r3cls,
                       r3.nscore          as r3score,
                       r4.nclassification as r4cls,
                       r4.nscore          as r4score,
                       r5.nclassification as r5cls,
                       r5.nscore          as r5score
                from r1
                inner join r2 on r2.table_name = r1.table_name and r2.column_name = r1.column_name
                inner join r3 on r3.table_name = r1.table_name and r3.column_name = r1.column_name
                inner join r4 on r4.table_name = r1.table_name and r4.column_name = r1.column_name
                inner join r5 on r5.table_name = r1.table_name and r5.column_name = r1.column_name
                where r1.oclassification <> 'EMPTY' and r1.nclassification is not null and r2.nclassification is not null and r3.nclassification is not null and r4.nclassification is not null """)

    res.coalesce(1).write.mode(SaveMode.Overwrite).option("header", true).csv(s"$mnt/pii-identification/cmp_runs")

    println("done")


  }
  test("final csv") {
    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
    val run = spark.read.option("header", true).csv(s"$mnt/pii-identification/cmp_runs")
    run.createOrReplaceTempView("all")
    spark.sql("select * from all where uatcls <> 'NONE' and r1cls <> 'NONE' and r2cls <> 'NONE' and r3cls <> 'NONE' and r4cls <> 'NONE' and r5cls <> 'NONE'").show()
    println("done")
  }

  test("pqt to csv"){
    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
    val run = spark.read.parquet(s"$mnt/pii-identification/summary6")
    run.coalesce(1).write.mode(SaveMode.Overwrite).option("header", true).csv(s"$mnt/pii-identification/last_run_csv")

    println("done")
  }

  test("read various pqt data") {
    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
    // lfsc360-test-exadata-au-svc-repuser-raw/ingest_exadata_au_svc_repuser/dm_acct_details/year=2021/month=03/day=23/LOAD00000001.parquet
    //lfsc360-test-exadata-au-svc-repuser-raw/pii-identification/sample-data/ingest_exadata_au_svc_repuser/activation/columns/LOYALTY_INDICATOR.parquet/part-00000-50147756-cd1a-41df-8b0c-1a0a129e3d99-c000.snappy.parquet
//    val run = spark.read.parquet(s"$mnt/ingest_exadata_au_svc_repuser/dm_acct_details/year=2021/month=03/day=23")
//    val run = spark.read.parquet(s"$mnt/pii-identification/sample-data/ingest_exadata_au_svc_repuser/activation/columns/LOYALTY_INDICATOR.parquet")
    val run = spark.read.parquet(s"$mnt/pii-identification/sample-data/ingest_exadata_au_svc_repuser/ech_exp_design/columns/CAMPAIGN_SEGMENT.parquet")
//    val na = run.select(trim(col("LOYALTY_INDICATOR")).as("LOYALTY_INDICATOR")).na.drop().filter(col("LOYALTY_INDICATOR").notEqual(lit("")))
    run.createOrReplaceTempView("act")
    spark.sql("select * from act").count()
    println("done")
  }

  test("read parquet") {
    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
    val table = "ech_exp_design".toLowerCase()
    val column = "CAMPAIGN_SEGMENT"
    spark.read.parquet(s"$mnt/pii-identification/sample-data/ingest_exadata_au_svc_repuser/$table/columns/$column.parquet").createOrReplaceTempView("peek")
    spark.sql("select * from peek").show(false)
    println("done")
  }

//38734 vs 5239481 1752 vs 122447
  //:443?warehouse=CUSTOMER360_SUPPORT_WH&db=C360&schema=PII_METADATA
  test("read from snowflake") {
    val pass = System.getenv("pass")

    import net.snowflake.spark.snowflake.Utils.SNOWFLAKE_SOURCE_NAME
    var sfOptions = Map(
      "sfURL" -> "customer360nonprod.ap-southeast-2.snowflakecomputing.com",
      "sfUser" -> "700005171@latitudefs.com",
      "sfPassword" -> s"$pass",
      "sfAuthenticator" -> "externalbrowser",
      "sfDatabase" -> "C360",
      "sfSchema" -> "PII_METADATA",
      "sfWarehouse" -> "CUSTOMER360_SUPPORT_WH"
    )

    val df = spark.read
      .format(SNOWFLAKE_SOURCE_NAME)
      .options(sfOptions)
      //      .option("dbtable", "PII_COLUMN_CLASSIFICATION_BASELINE")
      .option("query", "select * from C360.PII_METADATA.PII_COLUMN_CLASSIFICATION_BASELINE where LOAD_DATE = '2021-03-10'")
      .load()

    df.show(false)
    df.createOrReplaceTempView("bl")
    spark.sql("select * from bl")
    println("done")

    val mnt = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"

    val news = spark.read.parquet(s"$mnt/pii-identification/summary6")
    news.createOrReplaceTempView("g2")

    println("querying")

    val missed = spark.sql(
      """
         select bl.table_name, bl.column_name,
                       bl.EXPECTED_CLASSIFICATION,
                       bl.HASHING_REQUIRED,
                       g2.classification as CLASSIFICATION,
                       g2.score          as SCORE
                from bl
                left join g2 on upper(g2.table_name) = bl.table_name and g2.column_name = bl.column_name
                where bl.HASHING_REQUIRED = 'Y' and (g2.CLASSIFICATION in ('EMPTY', 'NONE') or g2.CLASSIFICATION  is null )
        """)
    missed.createOrReplaceTempView("missed")
    spark.sql("select * from missed").show(false)

    val missed_prev = spark.read
      .format(SNOWFLAKE_SOURCE_NAME)
      .options(sfOptions)
      //      .option("dbtable", "PII_COLUMN_CLASSIFICATION_BASELINE")
      .option("query",
        """
select bl.table_name, bl.column_name,
       bl.EXPECTED_CLASSIFICATION,
       bl.HASHING_REQUIRED,
       cls.classification as CLASSIFICATION,
       cls.score          as SCORE
from (select * from C360.PII_METADATA.PII_COLUMN_CLASSIFICATION_BASELINE where LOAD_DATE = '2021-03-10' ) bl
         left join (select *
                    from (select upper(TABLE_NAME) as TABLE_NAME,
                                 COLUMN_NAME,
                                 EFFECTIVE_DATE,
                                 CLASSIFICATION,
                                 SCORE,
                                 CLASSIFICATION_OVERRIDE,
                                 HASH_COLUMN,
                                 rank()
                                         over (partition by TABLE_NAME, COLUMN_NAME order by EFFECTIVE_DATE desc) as rank
                          from C360.PII_METADATA.PII_COLUMN_CLASSIFICATION) v1
                    where rank = 1) as cls
                   on bl.TABLE_NAME = cls.TABLE_NAME and bl.COLUMN_NAME = cls.COLUMN_NAME
    where bl.HASHING_REQUIRED = 'Y' and cls.CLASSIFICATION in ('EMPTY', 'NONE') ;
          """)
      .load()

    missed_prev.createOrReplaceTempView("missed_prev")
    spark.sql("select * from missed_prev").show(false)

    val extra = spark.sql(
      """
         select g2.CLASSIFICATION, count(g2.*)
                from bl
                left join g2 on upper(g2.table_name) = bl.table_name and g2.column_name = bl.column_name
                where bl.HASHING_REQUIRED = 'N' and g2.CLASSIFICATION not in ('EMPTY', 'NONE', 'DATE') group by g2.CLASSIFICATION order by g2.CLASSIFICATION
        """)

    extra.createOrReplaceTempView("extra")
    spark.sql("select * from extra").show(false)


    val extra_prev = spark.read
      .format(SNOWFLAKE_SOURCE_NAME)
      .options(sfOptions)
      .option("query",
        """
          |select cls.CLASSIFICATION, count(cls.*)
          |from (select * from C360.PII_METADATA.PII_COLUMN_CLASSIFICATION_BASELINE where LOAD_DATE = '2021-03-10' ) bl
          |         left join (select *
          |                    from (select upper(TABLE_NAME) as TABLE_NAME,
          |                                 COLUMN_NAME,
          |                                 EFFECTIVE_DATE,
          |                                 CLASSIFICATION,
          |                                 SCORE,
          |                                 CLASSIFICATION_OVERRIDE,
          |                                 HASH_COLUMN,
          |                                 rank()
          |                                         over (partition by TABLE_NAME, COLUMN_NAME order by EFFECTIVE_DATE desc) as rank
          |                          from C360.PII_METADATA.PII_COLUMN_CLASSIFICATION) v1
          |                    where rank = 1) as cls
          |                   on bl.TABLE_NAME = cls.TABLE_NAME and bl.COLUMN_NAME = cls.COLUMN_NAME
          |    where bl.HASHING_REQUIRED = 'N' and cls.CLASSIFICATION not in ('EMPTY', 'NONE', 'DATE') group by cls.CLASSIFICATION order by cls.CLASSIFICATION
          |""".stripMargin)
      .load()

    extra_prev.createOrReplaceTempView("extra_prev")

    spark.sql("select * from extra_prev").show(false)
    println("done")

  }
}


