package au.com.latitudefinancial

import java.util.Base64

import au.com.latitudefinancial.utils.DataEncryption.{aesEncrypt, randomIV}
import org.scalatest.FunSuite

class DataEncryptionTest extends FunSuite {
  val key = "ZGVyU2VjdXJpdHlQYXJvbA=="
  val iv = "0tAgC36ItVeMEwC9BweWwg=="

  test("DataEncryption should encrypt") {
    val input = "Data Engineering Coding Challenges"
    val encrypted = aesEncrypt(input, key, iv)

    assert(encrypted !== input)
  }

  test("return null while encrypting null") {
    val input: String = null
    assert(aesEncrypt(input, key, iv) === null)
  }

  test("generate 16 byte IV for AES") {
    val iv = randomIV()
    assert(Base64.getDecoder.decode(iv).length === 16)
  }
}
