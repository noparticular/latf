package au.com.latitudefinancial

import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions._
import org.scalatest.FunSuite

class TealiumFirehose extends FunSuite with SparkContextContainer {

//  val mnt = "s3a://lfscnpcom-datalake2-test-raw"
//  val mnt = "s3a://lfsc360-prod-tealium-eventstream-landing"
  val mnt = "s3a://lfsc360-prod-tealium-eventstream-raw"
  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "prglass")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  test("read a firehose dump") {
//    val src = spark.read.json(s"$mnt/tealium-visitor/year=2022/month=0*/*/*/*")
//    val src = spark.read.json(s"$mnt/ingest_tealium_eventstream/tealium_visitor/year=2022/month=02/day=*/*/*")
    val src = spark.read.json(s"$mnt/ingest_tealium_eventstream/tealium_visitor/year=2022/month=02/day=14/hour=19/")
    src.printSchema()
    src.createOrReplaceTempView("peek")
    spark.sql("select count(*) from peek").show(false)
    println(src.count())
//    spark.sql("desc formatted peek").show(false)
    println("done")
  }

  test("read json colum from snowflake") {
    val pass = System.getenv("pass")

    import net.snowflake.spark.snowflake.Utils.SNOWFLAKE_SOURCE_NAME
    var sfOptions = Map(
      "sfURL" -> "customer360prod.ap-southeast-2.snowflakecomputing.com",
      "sfUser" -> "700005171@latitudefs.com",
      "sfPassword" -> s"$pass",
      "sfAuthenticator" -> "externalbrowser",
      "sfDatabase" -> "RAW",
      "sfSchema" -> "TEALIUM_EVENTSTREAM"
    )

    import spark.implicits._
    val df = spark.read
      .format(SNOWFLAKE_SOURCE_NAME)
      .options(sfOptions)
      //      .option("dbtable", "TEALIUM_VISITOR")
      .option("query", "select JSON_COLUMN from RAW.TEALIUM_EVENTSTREAM.TEALIUM_VISITOR")
      .load().as[String]

    val json_schema = spark.read.json(df).schema
    json_schema.printTreeString()
//    import org.apache.spark.sql.functions.from_json
//    val jdf = df.withColumn("JSON_COLUMN", from_json($"JSON_COLUMN", json_schema))
//    jdf.printSchema()
//    jdf.createOrReplaceTempView("bl")
//    spark.sql("select * from bl").show()
    println("done")
  }

  //  Coping (1 of 11) s3://lfsc360-test-tealium-eventstream-landing/tealium-eventstream/tealium-prospect-nonprod/year=2022/month=02/day=07/hour=01/tealium-prospect-nonprod+0+0000057645.json to
  //  s3://lfsc360-test-tealium-eventstream-raw/ingest_tealium_eventstream/tealium_prospect/year=2022/month=02/day=07/hour=01/tealium-prospect-nonprod+0+0000057645.json
//  lfs${ServiceName}-${Configuration}-${AWSName}-landing
//  def getName = udf((path: String) => path.split('/').last)//.substring(18)
  def getName = udf((path: String) => path.substring(6))

  test("read csv sftp") {
    System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "dopr")

    val peek = spark.read.option("header", true).csv(s"s3a://datalake-prod-sftp-home/home/tealium/pull/customer-profile/daily/").withColumn("fname", getName(input_file_name()))
    peek.createOrReplaceTempView("sftp")


    val pass = System.getenv("pass")

    import net.snowflake.spark.snowflake.Utils.SNOWFLAKE_SOURCE_NAME
    var sfOptions = Map(
      "sfURL" -> "customer360prod.ap-southeast-2.snowflakecomputing.com",
      "sfUser" -> "700005171@latitudefs.com",
      "sfPassword" -> s"$pass",
      "sfAuthenticator" -> "externalbrowser",
      "sfDatabase" -> "RAW",
      "sfSchema" -> "TEALIUM_EVENTSTREAM"
    )

    import spark.implicits._
    val df = spark.read
      .format(SNOWFLAKE_SOURCE_NAME)
      .options(sfOptions)
      //      .option("dbtable", "TEALIUM_VISITOR")
      .option("query", """Select
                         |    JSON_COLUMN:current_visit.last_event.data.udo.customer_email_hashed as customer_email_hashed,
                         |    JSON_COLUMN:current_visit.last_event.data.udo.customer_master_id as customer_master_id,
                         |    JSON_COLUMN:current_visit.last_event.data.udo.customer_phone_hashed as customer_phone_hashed,
                         |    JSON_COLUMN:current_visit.last_event.data.udo._source_file as source_file,
                         |    JSON_COLUMN:current_visit.last_event.data.udo._file_line_num as file_line_num,
                         |    JSON_COLUMN:current_visit.last_event.type as import_type,
                         |    JSON_COLUMN:current_visit.last_event.data.udo.tealium_event as tealium_event
                         |from    raw.tealium_eventstream.tealium_visitor
                         |where   date(AIRFLOW_INSERT_TIME) >= '2022-02-10'
                         |  and     tealium_event = 'imported'
                         |  and     import_type = 'BULK_IMPORT'
                         |  and     source_file not like '%dailylfs-profile_delta_20220209_a.csv%'
                         |order by source_file, file_line_num""".stripMargin)
      .load()
    df.createOrReplaceTempView("kns")

    spark.sql("select * from kns").show(false)
    df.write.parquet(s"target/out/kns")


    //    spark.sql("desc peek").show(200, false)
    println("done")
  }

  test("compare sftp vs kinesis hashed") {
    import org.apache.spark.sql.functions._
    val sftp = spark.read.parquet("target/out/sftp")
    sftp.createOrReplaceTempView("sftp")
    val kns = spark.read.parquet("target/out/kns")
    kns.withColumn("CUSTOMER_MASTER_ID", col("CUSTOMER_MASTER_ID").substr(2,36)).createOrReplaceTempView("kns")

    spark.sql("select count(*) from sftp left anti join kns on kns.CUSTOMER_MASTER_ID = sftp.CUSTOMER_MASTER_ID").show(false)

    val res = spark.sql("select * from sftp left anti join kns on kns.CUSTOMER_MASTER_ID = sftp.CUSTOMER_MASTER_ID")
//    res.coalesce(1).write.mode(SaveMode.Overwrite).option("header",true).csv("target/out/res")
    res.write.mode(SaveMode.Overwrite).parquet("target/out/result")

    println("done")
  }

  test("share on sf") {
    val res = spark.read.parquet("target/out/result")

    val pass = System.getenv("pass")
    import net.snowflake.spark.snowflake.Utils.SNOWFLAKE_SOURCE_NAME
    var sfOptions = Map(
      "sfURL" -> "customer360prod.ap-southeast-2.snowflakecomputing.com",
      "sfUser" -> "700005171@latitudefs.com",
      "sfPassword" -> s"$pass",
      "sfAuthenticator" -> "externalbrowser",
      "sfRole" -> "CNP-ACCESS-PLATFORM-DATA-C360-DEV",
      "sfDatabase" -> "WS_CUSTOMER360_SUPPORT",
      "sfSchema" -> "CUSTOMER_MASTER"
    )

    res.write
      .format(SNOWFLAKE_SOURCE_NAME)
      .options(sfOptions)
      .option("dbtable", "CUSTOMER_MASTER.MISSING_RECORDS")
      .mode(SaveMode.Overwrite)
      .save()
  }

  test("risk sf role") {
    import spark.implicits._
    val df = spark.sparkContext.parallelize(1 to 10).toDF("Num")

    val pass = System.getenv("pass")
    import net.snowflake.spark.snowflake.Utils.SNOWFLAKE_SOURCE_NAME
    var sfOptions = Map(
      "sfURL" -> "customer360nonprod.ap-southeast-2.snowflakecomputing.com",
      "sfUser" -> "700005171@latitudefs.com",
      "sfPassword" -> s"$pass",
      "sfAuthenticator" -> "externalbrowser",
      "sfRole" -> "SPARK_RISK_MODELLING",
      "sfDatabase" -> "RISK_MODELLING",
      "sfSchema" -> "SCORECARD_SUMMARY"
    )

    df.write
      .format(SNOWFLAKE_SOURCE_NAME)
      .options(sfOptions)
      .option("dbtable", "RISK_MODELLING.SCORECARD_SUMMARY.LOCAL_EXMP")
      .mode(SaveMode.Overwrite)
      .save()
  }
}

