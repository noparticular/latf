package au.com.latitudefinancial

import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import org.apache.spark.sql.functions.input_file_name
import org.scalatest.FunSuite

class pcloReader extends FunSuite with SparkContextContainer {

  val mnt = "s3a://lfsc360-prod-exadata-raw"

  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "dopr")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  test("read a csv") {
    val peek = spark.read.option("header", true).csv("s3a://datalake-pclo-dataops-prod-workspace/enrolment/transfer/c360/DAILY_ENROLMENT_SNAPSHOT_20220125_OFFERDB.csv")
    peek.createOrReplaceTempView("peek")
    peek.printSchema()
    spark.sql("select count(*) from peek where ACCOUNT_NUMBER in('5218933534008288','5218933534008726')").show(false)
    spark.sql("select * from peek").show(false)
    println("done")
  }

  test("read a folder") {
    val peek = spark.read.option("header", true).csv("s3a://datalake-pclo-dataops-prod-workspace/enrolment/transfer/c360/*_OFFERDB.csv").withColumn("fname", input_file_name())
    peek.createOrReplaceTempView("peek")
    peek.printSchema()
    spark.sql("select * from peek where ACCOUNT_NUMBER in('5218933534008288','5218933534008726')").show(false)
    println("done")
    peek.explain()
  }

}