package au.com.latitudefinancial

import java.sql.Date

import au.com.latitudefinancial.utils.DataGeneration
import org.scalatest.{BeforeAndAfterAll, FunSuite}

class DataGenerationTest extends FunSuite with BeforeAndAfterAll {

  private val magicSeed = 0.042
  private val magicName = "㳻"
  private val anotherMagicSeed = 8
  private val arbitraryNameLength = 12
  val minDate: Date = Date.valueOf("1900-01-01")
  val maxDate: Date = Date.valueOf("2020-01-01")

  test("generate date should put date between provided range") {
    val res = DataGeneration.generateDate(magicSeed, minDate, maxDate)
    assert(res.compareTo(minDate) >= 0)
    assert(res.compareTo(maxDate) < 0)
  }

  test("generated address could contain more characters") {
    val ordinary = ('a' to 'z') ++ ('A' to 'Z') ++ ('0' to '9')
    val name = DataGeneration.generateAddress(magicSeed, arbitraryNameLength)
    assert(!name.forall(ordinary.contains(_)))
  }

  test("generated names length should be positive") {
    val (_, len) = DataGeneration.init(magicSeed, DataGeneration.shortestNameLength, 0)
    assert(len > 0)
  }

  test("generated names should be same for provided seed") {
    val name = DataGeneration.generateName(magicSeed, arbitraryNameLength)
    assert(name === magicName)
  }


  test("generated names should not be empty") {
    val name = DataGeneration.generateName(magicSeed, 0)
    assert(!name.isEmpty)

  }

  test("generated names should not be same for different seeds") {
    val name1 = DataGeneration.generateName(magicSeed, arbitraryNameLength)
    val name2 = DataGeneration.generateName(anotherMagicSeed, arbitraryNameLength)
    assert(name1 !== name2)
  }
  test("generated names should be same for same seeds") {
    val name1 = DataGeneration.generateName(magicSeed, arbitraryNameLength)
    val name2 = DataGeneration.generateName(magicSeed, arbitraryNameLength)
    assert(name1 === name2)
  }

}
