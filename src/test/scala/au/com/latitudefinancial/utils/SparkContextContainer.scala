package au.com.latitudefinancial.utils

import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.scalatest.{BeforeAndAfterAll, Suite}

trait SparkContextContainer extends BeforeAndAfterAll {

  this: Suite =>

  val conf: SparkConf = new SparkConf()
    .setAppName("Running Data Test")
    .set("spark.sql.legacy.parquet.datetimeRebaseModeInRead", "CORRECTED")
    .set("spark.sql.caseSensitive", "true")
    .setMaster("local[*]")
  implicit lazy val spark: SparkSession = SparkSession.builder().config(conf).getOrCreate()
  scala.sys.addShutdownHook {
    spark.stop()
  }


  override protected def beforeAll(): Unit = {
    val sc = spark.sparkContext
    val rootLogger = Logger.getRootLogger
    rootLogger.setLevel(Level.ERROR)
  }
}
