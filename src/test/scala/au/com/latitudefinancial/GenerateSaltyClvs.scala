package au.com.latitudefinancial

import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{DataType, StringType}
import org.scalatest.FunSuite

class GenerateSaltyClvs extends FunSuite with SparkContextContainer with EtlApp {
  import spark.implicits._

  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "glass")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")
  val mnt = "s3a://lfsc360-test-c360-sandbox-export"

  val lcl = "src/test/resources"

/*

  test("generate dataframe with salt columns"){
    val base = spark.read.parquet(s"$lcl/AU_SVC_REPUSER.DM_ACCT_DETAILS.parquet")

    val res = base.select($"CLV_CUSTOMER_ID".cast(StringType)).distinct().withColumn("CUSTOMER_MASTER_ID", lit("221339a4-b33d-4f5d-a5d4-f7dd610f48a8")).withColumn("SALT", randomIVUdf()).withColumn("INSERT_DATETIME", functions.current_timestamp()).withColumn("UDATE_DATETIME", functions.current_timestamp())
    res.printSchema()
    res.show(false)
    res.repartition(1).write.mode(SaveMode.Overwrite).parquet(s"$mnt/export_c360_sandbox_pii_metadata/customer_master_clv_salt.parquet")

  }
*/

  test("get one") {
    println(s"profile = ${System.getProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY)}")
    val raw = "s3a://lfsc360-test-exadata-au-svc-repuser-raw"
//    spark.conf.set("spark.sql.legacy.parquet.datetimeRebaseModeInRead", "LEGACY")
    spark.conf.set("spark.sql.legacy.parquet.datetimeRebaseModeInRead", "CORRECTED")
    val src = spark.read.parquet(s"$raw/ingest_exadata_au_svc_repuser/dm_acct_details/year=2021/month=03/day=10/")
    src.createOrReplaceTempView("src")
    spark.sql("select * from src")
    src.show(false)
  }

  test("read parquet") {
    val src = spark.read.parquet(s"s3a://lfsc360-dev-exadata-au-svc-repuser-raw/sample/ingest_exadata_au_svc_repuser/pii_table/columns/MOBILENUMBER.parquet")
    src.createOrReplaceTempView("src")
    src.show(false)
    val full = spark.read.parquet(s"s3a://lfsc360-dev-exadata-au-svc-repuser-raw/ingest_exadata_au_svc_repuser/pii_table/year=2021/month=03/day=15/AU_SVC_REPUSER.DM_ACCT_DETAILS.parquet")
    full.createOrReplaceTempView("full")
    spark.sql("select distinct MOBILENUMBER from full").show()
    spark.sql("select count(MOBILENUMBER) from full").show()

  }


}
