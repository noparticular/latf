package au.com.latitudefinancial

import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.col
import org.scalatest.FunSuite
import utils.SparkContextContainer

class EtlAppTest extends FunSuite with SparkContextContainer with EtlApp {

  import spark.implicits._

  val encryptionKey: String = "ZGVyU2VjdXJpdHlQYXJvbA=="

  val inputDf: DataFrame =
    Seq(
      (1, "Name1", "Surname1", "Address1", "1942-10-04"),
      (2, "Name2", "Surname2", "Address2", "1937-10-11"),
      (3, "Name3", "Surname3", "Address3", "1949-06-15")
    ).toDF("id", "first_name", "last_name", "address", "date_of_birth")

  test("Anonimizing should encrypt specified columns") {
    val encryptedColumns = List("first_name", "last_name", "address")
    val encryptedDf = anonymizeDataFrame(inputDf, encryptedColumns, key = encryptionKey)

    val inputData = inputDf.select(encryptedColumns.map(c => col(c)): _*).collect()
    val actualRes = encryptedDf.select(encryptedColumns.map(c => col(c)): _*).collect()

    assert(actualRes.length === inputData.length)
    assert(actualRes.forall(r => !(inputData contains r)))
  }

  test("Anonimizing should not encrypt non-specified columns") {
    val encryptedColumns = List("first_name", "last_name", "address")
    val otherColumns = List("id", "date_of_birth")
    val encryptedDf = anonymizeDataFrame(inputDf, encryptedColumns, key = encryptionKey)

    val inputData = inputDf.select(otherColumns.map(c => col(c)): _*).collect()
    val actualRes = encryptedDf.select(otherColumns.map(c => col(c)): _*).collect()

    assert(actualRes.length === inputData.length)
    assert(actualRes.forall(inputData contains _))
  }

  test("Anonimizing should retain dataframe structure") {
    val encryptedColumns = List("first_name", "last_name", "address")
    val encryptedDf = anonymizeDataFrame(inputDf, encryptedColumns, key = encryptionKey)
    assert(encryptedDf.count() === inputDf.count())
    assert(inputDf.columns.forall(c => encryptedDf.columns.contains(c)))
  }
}

