import au.com.latitudefinancial.utils.DataGeneration.{generateAddress, generateDate, generateId, generateName}
import au.com.latitudefinancial.utils.SparkContextContainer
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{lit, udf, _}
import org.scalatest.FunSuite
import java.sql.Date

import au.com.latitudefinancial.utils.DataEncryption.randomIV

import scala.collection.immutable


class PiiShuffleGeneratorRunner extends FunSuite with SparkContextContainer {


//  val numberOfRows = 2L
  val numberOfRows = 12345L
  //  val numberOfRows = 1234567L

  val pcThreadsNumber = 8

//  val mnt = "src/test/resources"
  import spark.implicits._
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")
  val mnt = "s3a://lfsc360-dev-exadata-au-svc-repuser-landing/AU_SVC_REPUSER"

  def genId: UserDefinedFunction = udf[Long, Double](generateId)

  def randomIVUdf: UserDefinedFunction = udf[String](randomIV)


  test("generate csv") {
    val src = spark.read.parquet(s"$mnt/PII_TABLE/AU_SVC_REPUSER.DM_ACCT_DETAILS.parquet")

    val ids: immutable.Seq[Long] = (0L to numberOfRows)

    val baseDf = src.withColumn("rid", genId(randn())).withColumn("rnid", col("CLV_CUSTOMER_ID").bitwiseXOR(col("rid"))).drop("CLV_CUSTOMER_ID", "rid").withColumnRenamed("rnid", "CLV_CUSTOMER_ID")
    val rndDF = spark.sparkContext.parallelize(ids, pcThreadsNumber).toDF("id").withColumn("rid", genId(randn())).drop("id")
    val gen = baseDf.crossJoin(rndDF).withColumn("rnid", col("CLV_CUSTOMER_ID").bitwiseXOR(col("rid"))).drop("CLV_CUSTOMER_ID", "rid").withColumnRenamed("rnid", "CLV_CUSTOMER_ID")
    gen.show()

    val salt = gen.select($"CLV_CUSTOMER_ID").distinct().withColumn("SALT", randomIVUdf())

    gen.repartition(64).write.mode(SaveMode.Overwrite).parquet(s"$mnt/GEN_TABLE")
    val mntraw="s3a://lfsc360-dev-exadata-au-svc-repuser-raw"
    salt.repartition(32).write.mode(SaveMode.Overwrite).parquet(s"$mntraw/gen_salt")
    println(s"gen.count() = ${gen.count()}")
  }

}
