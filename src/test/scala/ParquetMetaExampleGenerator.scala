import java.sql.{Date, Timestamp}

import au.com.latitudefinancial.utils.SparkContextContainer
import org.apache.spark.sql.{Encoders, SaveMode}
import org.apache.spark.sql.functions._
import org.scalatest.FunSuite

case class column_classification(
                                  data_source_name: String = "asr",
                                  database_name: String = "exadata",
                                  table_name: String = "pii_table",
                                  column_name: String,
                                  classification: String,
                                  score: Double,
                                  classification_date: Timestamp = new Timestamp(System.currentTimeMillis()),
                                  classification_override: String = "",
                                  hash_column: Boolean,
                                  column_criteria: String = null,
                                  file_path: String = null,
                                  airflow_insert_time: Timestamp = null,
                                  airflow_dag_time: Timestamp = null
                                )


class ParquetMetaCleanser extends FunSuite with SparkContextContainer {

  import spark.implicits._

  val ddl_schema = "`data_source_name` STRING,`table_name` STRING,`column_name` STRING,`classification` STRING,`score` DOUBLE,`classification_date` DATE,`manual_classification` STRING,`hash_column` BOOLEAN,`column_criteria` STRING"

  test("print schema") {
    val classificationSchema = Encoders.product[column_classification].schema
    classificationSchema.printTreeString()
  }

  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  test("generate asr parquet sample") {
    val mnt = "s3a://lfsc360-dev-exadata-asr-raw/ingest_exadata_asr"
    val lcl = "target/out"

    val meta = spark.read.json(s"$mnt/pii_table/columns-classification/cache.jsonl")
    val cols: Array[String] = meta.columns

    val res = meta.select(
      explode(array(cols.map(cname => struct(lit(cname).alias("key"), col(cname).alias("val"))): _*))
    ).select(
      $"col.key".as("column_name"),
      $"col.val.Entity".as("classification"),
      $"col.val.Score".as("score")
    )

    val fullres = res.map(row => {
      column_classification(
        column_name = row.getAs[String]("column_name"),
        classification = row.getAs[String]("classification"),
        score = row.getAs[Double]("score"),
        hash_column = row.getAs[Double]("score") > 0.2
      )
    })

    fullres.printSchema()
//    fullres.write.mode(SaveMode.Overwrite).parquet(s"$mnt/pii_table/columns-classification/pqt")
    fullres.write.mode(SaveMode.Overwrite).parquet(s"$lcl/asr")


  }

}
