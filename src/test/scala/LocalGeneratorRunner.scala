import java.sql.Date

import au.com.latitudefinancial.utils.SparkContextContainer
import au.com.latitudefinancial.utils.DataGeneration.{generateAddress, generateDate, generateName}
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.expressions.UserDefinedFunction
import org.apache.spark.sql.functions.{lit, udf, _}
import org.scalatest.FunSuite

import scala.collection.immutable


class LocalGeneratorRunner extends FunSuite with SparkContextContainer {

  import spark.implicits._

  val numberOfRows = 1234567L

  val pcThreadsNumber = 8

  val mnt = "src/test/resources"

  def genName: UserDefinedFunction = udf[String, Double, Int](generateName)

  def genAddress: UserDefinedFunction = udf[String, Double, Int](generateAddress)

  def genDate: UserDefinedFunction = udf[Date, Double, Date, Date](generateDate)

  test("generate csv") {
    val nameLengthBound = 18
    val addressLengthBound = 32
    val minDate: Date = Date.valueOf("1900-01-01")

    val ids: immutable.Seq[Long] = (0L to numberOfRows)

    val baseDF = spark.sparkContext.parallelize(ids, pcThreadsNumber).toDF("id")

    val genData = baseDF.withColumn("first_name", genName(randn(), lit(nameLengthBound)))
      .withColumn("last_name", genName(randn(), lit(nameLengthBound)))
      .withColumn("address", genAddress(randn(), lit(addressLengthBound)))
      .withColumn("date_of_birth", genDate(randn(), lit(minDate), current_date()))

    genData.write.mode(SaveMode.Overwrite).option("header", value = true).csv(s"$mnt/generated.csv")
  }
}
