import org.scalatest.FunSuite
import au.com.latitudefinancial.utils.SparkContextContainer
import org.apache.spark.sql.SaveMode


class SamplingTest extends FunSuite with SparkContextContainer {
  //read aws creds from ~/.aws
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  //test seed parameters
  val trueSeed = 42
  val percentFraction = 0.01

  test("sample locally") {
    val mnt = "s3a://lfsc360-dev-exadata-asr-raw"
    val lcl = "src/test/resources"

    val partitioned = spark.read.parquet(s"$mnt/ingest_exadata_asr/pii_table/year*")

    partitioned.describe().show(false)
    val pSample = partitioned.sample(percentFraction, trueSeed)
    pSample.coalesce(1).write.mode(SaveMode.Overwrite).parquet(s"$lcl/pSample")
    pSample.describe().show(false)
  }
}