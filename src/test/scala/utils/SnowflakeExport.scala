package utils

import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import org.apache.spark.sql.SaveMode
import org.scalatest.FunSuite

class SnowflakeExport extends FunSuite with SparkContextContainer {

// glass setup to read form s3
  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "glass")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")
  val mnt = "s3a://lfsc360-test-exadata-clv-raw"

//  sf write with admin role setup
  import net.snowflake.spark.snowflake.Utils.SNOWFLAKE_SOURCE_NAME

  val pass = System.getenv("pass")
  var sfOptions = Map(
    "sfURL" -> "customer360nonprod.ap-southeast-2.snowflakecomputing.com",
    "sfUser" -> "700005171@latitudefs.com",
    "sfPassword" -> s"$pass",
    "sfRole" -> "CNP-ACCESS-SNOWFLAKE-DATA-C360-ADMIN",
    "sfAuthenticator" -> "externalbrowser",
    "sfDatabase" -> "C360",
    "sfSchema" -> "PII_METADATA",
    "sfWarehouse" -> "CUSTOMER360_SUPPORT_WH"
  )

  test("export pii_column_classification to c360") {

    val res = spark.read.parquet(s"$mnt/pii-identification/metadata/PII_COLUMN_CLASSIFICATION")
    res.write
      .format(SNOWFLAKE_SOURCE_NAME)
      .options(sfOptions)
      .option("dbtable", "PII_COLUMN_CLASSIFICATION_CLV")
      .mode(SaveMode.Overwrite)
      .save()
  }
}
