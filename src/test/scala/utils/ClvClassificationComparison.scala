package utils

import java.sql.Timestamp

import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.functions._
import org.scalatest.FunSuite


case class column_classification(
                                  data_source_name: String = null,
                                  database_name: String = null,
                                  schema_name: String = "clv",
                                  table_name: String ,
                                  column_name: String,
                                  effective_date: Timestamp = null,
                                  classification: String = null,
                                  score: Double = 0,
                                  classification_override: String = null,
                                  classification_override_reason_code: String = null,
                                  classification_override_date: Timestamp = null,
                                  hash_column: String = null,
                                  column_criteria: String = null,
                                  file_path: String = null,
                                  airflow_insert_time: Timestamp = null,
                                  airflow_dag_time: Timestamp = null
                                )

class ClvClassificationComparison extends FunSuite with SparkContextContainer {

  import spark.implicits._
  val mnt = "s3a://lfsc360-test-exadata-clv-raw"

  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "prglass")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")


  test("read a parquet") {
    spark.read.parquet(s"s3a://lfsc360-prod-exadata-raw/pii-identification/au_svc_repuser/pii_column_classification.parquet").createOrReplaceTempView("peek")
//    s3a://lfsc360-test-exadata-raw/pii-identification/au_svc_repuser/pii_column_classification.parquet
    spark.sql("select * from peek").show(false)
    spark.sql("select schema_name, count(*) from peek group by schema_name").show(false)
    println("done")
  }


  test("read parquet") {
    val table = "crmref_account_base".toLowerCase()
    val column = "VCHBMPHYSICALADDRESS4"
    spark.read.parquet(s"$mnt/pii-identification/sample-data/ingest_exadata_clv/$table/columns/$column.parquet").createOrReplaceTempView("peek")
    spark.sql("select * from peek").show(false)
    println("done")
  }


  def getName = udf((path: String) => path.split('/').init.last)

  test("flatten clv classification") {

    val rs = spark.read.json(s"$mnt/pii-identification/metadata/ingest_exadata_clv/*/classification.jsonl").withColumn("tname", input_file_name())
    rs.select(col("tname")).show(false)
    val cols: Array[String] = rs.drop("tname").columns
    val exp = rs.select($"tname",
      explode(array(cols.map(cname => struct(lit(cname).alias("key"), col(cname).alias("val"))): _*))
    ).select(
      getName($"tname").as("table_name"),
      $"col.key".as("column_name"),
      $"col.val.Entity".as("classification"),
      $"col.val.Score".as("score")
    ).where($"classification".isNotNull.and($"score".isNotNull))

    exp.createOrReplaceTempView("clsf")
    val rescount = spark.sql("select * from clsf").count()
    println(s"res_count = ${rescount}") //650

    exp.coalesce(1).write.mode(SaveMode.Overwrite).parquet(s"$mnt/pii-identification/metadata/summary")
    exp.coalesce(1).write.mode(SaveMode.Overwrite).option("header", true).csv(s"$mnt/pii-identification/metadata/summary.csv")
    println(s"done")
  }


  test("flatten clv overrides") {
    val lcl = "src/test/resources"

    val data = spark.read.option("multiline", true).json(s"$lcl/clv_overrides.json")

    val res = data.select($"table_name", explode($"schema").as("carr")).select(
      $"table_name",
      $"carr.column_name".as("column_name"),
      $"carr.classification_override".as("classification_override"),
      $"carr.classification_override_reason_code".as("classification_override_reason_code"),
    )

    res.createOrReplaceTempView("all")
    val rescount = spark.sql("select * from all").count()
    println(s"res_count = ${rescount}") //121

//    res.coalesce(1).write.mode(SaveMode.Overwrite).option("header", true).csv(s"$mnt/pii-identification/metadata/clv_overrides")
    res.coalesce(1).write.mode(SaveMode.Overwrite).option("header", true).csv(s"$lcl/clv_overrides")
  }

  test("write classification to pii colum classification shape") {
    val lcl = "src/test/resources"
    val tables = spark.read.option("multiline", true).json(s"$lcl/clv_overrides.json").select($"table_name").collect().map(_.getString(0).toUpperCase)
    val land = "s3a://lfsc360-test-exadata-clv-landing"
    val fullColumns = for(tname <- tables) yield (tname, spark.read.parquet(s"$land/CLV/$tname").columns)
    val fullDf = fullColumns.toSeq.toDF("table_name", "columns").select($"table_name".as("tname"), explode($"columns").as("cname"))
    fullDf.createOrReplaceTempView("fl")
    val clsummary = spark.read.parquet(s"$mnt/pii-identification/metadata/summary")
    val moverrides = spark.read.option("header", true).csv(s"$lcl/clv_overrides")
    clsummary.createOrReplaceTempView("cl")
    moverrides.createOrReplaceTempView("ov")

    val res = spark.sql(
      """
        |select fl.*, cl.*, ov.classification_override, ov.classification_override_reason_code
        |from fl
        |left join cl on lower(cl.table_name) = lower(fl.tname) and lower(cl.column_name) = lower(fl.cname)
        |left join ov on lower(cl.table_name) = lower(ov.table_name) and lower(cl.column_name) = lower(ov.column_name) order by cl.classification""".stripMargin)

    val fullres = res.map(row => {
      column_classification(
        table_name = row.getAs[String]("tname"),
        column_name = row.getAs[String]("cname"),
        classification = row.getAs[String]("classification"),
        score = row.getAs[Double]("score"),
        classification_override = row.getAs[String]("classification_override"),
        classification_override_reason_code = row.getAs[String]("classification_override_reason_code"),
      )
    })
    fullres.printSchema()
    fullres.show(false)

    fullres.write.mode(SaveMode.Overwrite).parquet(s"$mnt/pii-identification/metadata/PII_COLUMN_CLASSIFICATION")
  }

  test("compare classification vs overrides"){
    val clsummary = spark.read.parquet(s"$mnt/pii-identification/metadata/summary")
    val moverrides = spark.read.option("header", true).csv(s"$mnt/pii-identification/metadata/clv_overrides")
    clsummary.createOrReplaceTempView("cl")
    moverrides.createOrReplaceTempView("ov")

    val full = spark.sql("select cl.*, ov.table_name as ot, ov.column_name as oc from cl left join ov on lower(cl.table_name) = lower(ov.table_name) and lower(cl.column_name) = lower(ov.column_name) order by cl.classification")
    println(s"full.count() = ${full.count()}")
    println(s"ov.count() = ${moverrides.count()}")

    val same = full.where($"ot".isNotNull and not($"classification".isInCollection(Seq("NONE","EMPTY"))))
    println(s"same.count() = ${same.count()}")
    same.show(60,false)

    val miss = full.where($"ot".isNotNull and $"classification".isInCollection(Seq("NONE","EMPTY")))
    println(s"miss.count() = ${miss.count()}")
    miss.show(70,false)

    val newfound = full.where($"ot".isNull and not($"classification".isInCollection(Seq("NONE","EMPTY"))))
    println(s"newfound.count() = ${newfound.count()}")
    newfound.show(false)
    newfound.groupBy($"classification").count().show(false)

    val empty = full.where($"classification".eqNullSafe("EMPTY"))
    println(s"empty.count() = ${empty.count()}")
    empty.show(false)
    println("done")

  }
}