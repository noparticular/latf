package utils

import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import org.scalatest.FunSuite

class ParquetReaderTemplate extends FunSuite with SparkContextContainer {

  import spark.implicits._

  val mnt = "s3a://lfsc360-prod-exadata-raw"

  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "prglass")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  test("read a parquet") {
    //lfsc360-prod-exadata-cleansed/ingest_exadata/au_svc_repuser/dm_acct_card/year=2021/month=06/day=20/
    spark.read.parquet(s"s3a://lfsc360-prod-exadata-cleansed/ingest_exadata/au_svc_repuser/dm_acct_card/year=2021/month=06/day=20").createOrReplaceTempView("peek")
    //  s3a://lfsc360-test-exadata-raw/pii-identification/au_svc_repuser/pii_column_classification.parquet
    spark.sql("select calls_mobileapp_function_flag, speak_to_agent_l12m_flag, last_speak_to_agent_date from peek").show(false)
    spark.sql("select schema_name, count(*) from peek").show(false)
    println("done")
  }

  test("read raw bucket") {
    spark.read.parquet(s"s3a://lfsc360-prod-exadata-raw/ingest_exadata/clv/clv_src_party_cleanse/year=2021/month=06/day=22/").createOrReplaceTempView("peek")
    spark.sql("select * from peek").show(false)
    spark.sql("desc peek").show(200, false)
    println("done")
  }

  test("read cleansed bucket") {
    //    lfsc360-prod-exadata-cleansed/ingest_exadata/clv/clv_src_party_cleanse/year=2021/month=06/day=22/
    val src = spark.read.parquet(s"s3a://lfsc360-prod-lpay-genoapay-cleansed/ingest_genoapay/Address/year=2021/month=10/day=13/")
    src.printSchema()
    src.createOrReplaceTempView("peek")
    spark.sql("select * from peek").show(false)
  }

  test("look at field sample"){
    val src = spark.read.parquet("s3a://lfsc360-prod-lpay-genoapay-cleansed/ingest_genoapay/Address/year=2021/month=10/day=13/")
    src.printSchema()
    src.createOrReplaceTempView("peek")
    spark.sql("select streetsuffix, unitnumber, buildingname, unittype, streettype, streetnumber1, addresssource, streetname, leveltype, levelnumber from peek where streetsuffix is not null or unitnumber is not null or buildingname is not null or unittype is not null or streettype is not null or streetnumber1 is not null or addresssource is not null or streetname is not null or leveltype is not null or levelnumber is not null").show(false)
  }

  test("look at evari json"){
//    val src = spark.read.json("s3a://lfsc360-dev-evari-insurance-raw/ingest_evari_insurance/evari_insurance/year=2021/month=10/day=13/hour=05/")
    val src = spark.read.json(
      "s3a://lfsc360-dev-evari-insurance-raw/ingest_evari_insurance/evari_insurance/year=2021/month=10/day=08/hour=03/")
    src.printSchema()
    src.createOrReplaceTempView("peek")
    spark.sql("select  count(*) from peek ").show(false)
  }
}