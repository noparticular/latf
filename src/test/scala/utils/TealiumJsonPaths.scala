package utils

import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import com.amazonaws.services.gluedatabrew.model.JsonOptions
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.execution.datasources.json.MultiLineJsonDataSource
import org.apache.spark.sql.types.{StructField, StructType}
import org.scalatest.FunSuite

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

class TealiumJsonPaths extends FunSuite with SparkContextContainer {

  val lcl = "src/test/resources"
//  val mnt = "s3a://lfscnpcom-datalake2-prod-raw/tealium"
  val mnt = "s3a://lfsc360-prod-tealium-eventstream-landing/tealium-eventstream/tealium-prod"

  val sc: SparkContext = spark.sparkContext

  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "prglass")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  def flattenSchema(schema: StructType, pref: String): Seq[String] = {
    schema.fields.flatMap {
      case StructField(name, inner: StructType, _, _) => flattenSchema(inner, s"$pref$name.")
      case StructField(name, _, _, _) => Seq(s"$pref$name")
    }
  }

  test("read json"){
    val rpath = s"$mnt/year=2021/month=08/day=26/hour=01/"

//    val src = spark.read.json(sc.wholeTextFiles(rpath).flatMap(x => x._2.replaceAll("}\\{", "}#!#{").split("#!#")))
    val src = spark.read.json(rpath)
    src.createOrReplaceTempView("peek")

    spark.sql("select count(*) from peek where data.udo.is_production = true").show()

    val sql1 =
      """ select * from peek where
        |data.udo.page_pagetype = 'quote-page'
        |AND
        |data.udo.is_production = true
        |AND
        |data.udo.logical_event_trigger = 'contact-details-added'
        |""".stripMargin
    val sql2 =
      """ select * from peek where
        |data.udo.page_pagetype = 'quote-page'
        |AND
        |data.udo.is_production = true
        |AND
        |data.udo.logical_event_trigger = 'quote-submit'
        |""".stripMargin
    val res1 = spark.sql(sql1).limit(1)
    println(res1.count())
    val res2 = spark.sql(sql2).limit(1)

    res1.write.json(s"$lcl/out1.json")
    res2.write.json(s"$lcl/out2.json")

    println("done")
  }

  test("dump column names") {
    val lpath = s"$lcl/tealium/*.json"

    val rpath = s"$mnt/year=2021/month=08/day=25"

//    val src = spark.read.json(sc.wholeTextFiles(rpath).flatMap(x => x._2.replaceAll("}\\{", "}#!#{").split("#!#")))
    val src = spark.read.json(rpath)
    val columns = flattenSchema(src.schema, "")
    columns.sorted.foreach(println)
    Files.write(Paths.get(f"$lcl/t0825.txt"), columns.mkString("\n").getBytes(StandardCharsets.UTF_8))
    println("done")
  }

  test("read with transform"){
    import spark.implicits._
    val preText = sc.wholeTextFiles(s"$lcl/tealium/*.json").flatMap(x => x._2.replaceAll("}\\{", "}#!#{").split("#!#")).toDS()
    val src = spark.read.json(preText)
    println(src.count())
  }

  test("read remote gz") {
    val folder = "year=2021/month=09/day=25/hour=05"
    val spath = s"$mnt/$folder/"

    val src = spark.read.json(spath)
    println(src.count())
  }
}