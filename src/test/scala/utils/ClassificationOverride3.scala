package utils

import au.com.latitudefinancial.utils.SparkContextContainer
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Column, DataFrame, SaveMode}
import org.apache.spark.sql.functions._
import org.scalatest.FunSuite

class ClassificationOverride3 extends FunSuite with SparkContextContainer {

  import spark.implicits._

//  how to instruction
//  copy python source/s into $base/in as is
//  copy override csv into $base/in
//  adjust prepareOverrides mapping function if needed
//  run "convert pyobj to json"
//  run "merge classification with overrides"
//  run "rename output files"
//  create and run converter.py from out folder

  val mnt = "src/test/resources"
  val base = s"$mnt/co1609"
  val override_path = s"$base/in/override_request_20210916.csv"

  private def prepareOverrides(srcDf: DataFrame) = {
    val res = srcDf
      .withColumn("schema", lower(col("SCHEMA_NAME")))
      .withColumn("table_name", lower(col("TABLE_NAME")))
      .withColumn("column_name", upper(col("COLUMN_NAME")))
      .withColumn("classification_override", upper(col("CLASSIFICATION_OVERRIDE")))
      .withColumn("classification_override_reason_code", upper(col("CLASSIFICATION_OVERRIDE_REASON_CODE")))
      .select("schema", "table_name", "column_name", "classification_override", "classification_override_reason_code")
    //    res.printSchema()
    //    res.show()
    res
  }

  test("convert pyobj to json") {
    def getName = udf((path: String) => path.split('/').last)

    def removeTablePrefix(col: Column): Column = regexp_replace(col, """^(TABLES|TARGET_TABLES)\s*=\s*\\?\s*""", "")

    def replaceSingleQuotesWithDouble(col: Column): Column = regexp_replace(col, "\'", "\"")

    def cleanLastCommaInArray(col: Column): Column = regexp_replace(col, """\s*,\s*]$""", "]")

    val raw = spark.sparkContext.wholeTextFiles(s"$base/in/*.py").toDF("fn", "pyobj")
    val pyObj = raw.select(
      getName($"fn").as("src"),
      removeTablePrefix($"pyobj").as("body")
    )
    val json = pyObj.select($"src", cleanLastCommaInArray(replaceSingleQuotesWithDouble($"body")).as("json_body"))
    json.write.mode(SaveMode.Overwrite).partitionBy("src").text(s"$base/in/js")
  }

  test("merge classification with overrides") {
    val clJson = spark.read.option("multiLine", true)
      .json(s"$base/in/js/*/*.txt").withColumn("fname", input_file_name())

    val flatBase = flattenTables(clJson)
    flatBase.createOrReplaceTempView("cl")

    val schemaToPath = clJson.select(getSrcName($"fname").as("path"), getSchema(getSrcName($"fname")).as("schema")).distinct()

    val newOverridesCsv = spark.read.option("header", true).csv(override_path)

    val newOverrides = prepareOverrides(newOverridesCsv)
    newOverrides.createOrReplaceTempView("ov")

    val updateRes = spark.sql(
      """
        |select cl.schema, cl.table_name, cl.table_size, cl.column_name,
        |coalesce(ov.classification_override,cl.classification_override) as classification_override,
        |coalesce(ov.classification_override_reason_code,cl.classification_override_reason_code) as classification_override_reason_code
        |from cl
        |left join ov on cl.schema = ov.schema and cl.table_name = ov.table_name and cl.column_name = ov.column_name""".stripMargin)
//    updateRes.show(false)
    println(s"updateRes.count() = ${updateRes.count()}")

    val insertRes = spark.sql(
      """
        |select ov.schema, ov.table_name, cast(null as string) as table_size,
        |ov.column_name, ov.classification_override, ov.classification_override_reason_code
        |from ov
        |left anti join cl on cl.schema = ov.schema and cl.table_name = ov.table_name and cl.column_name = ov.column_name""".stripMargin)

//    insertRes.show(false)
    println(s"insertRes.count() = ${insertRes.count()}")

    val collapseRes: DataFrame = combineOverrides(updateRes, insertRes)

    val finalRes = collapseRes.join(schemaToPath, "schema").select($"path", $"table_name", $"table_size", $"sch".as("schema"))
//    finalRes.printSchema()
//    finalRes.show()
    println(s"finalRes.count()  = ${finalRes.count()}")

    finalRes.repartition($"path").write.mode(SaveMode.Overwrite).partitionBy("path").json(s"$base/out/raw")
  }

  test("rename output files") {
    val jsDir = s"$base/out/raw"

    import org.apache.hadoop.fs._
    val fs = FileSystem.get(spark.sparkContext.hadoopConfiguration)
    val pathes = fs.globStatus(new Path(s"$jsDir/*/part*"))
    for {
      path <- pathes
      srcName = path.getPath.getName
      parName = path.getPath.getParent.getName
      destName = parName.substring("path=".length)
    } fs.rename(new Path(s"$jsDir/$parName/$srcName"), new Path(s"$base/out/rawj/$destName.json"))

//    fs.delete(new Path(s"$base/out/.*.crc"), true)
  }

  /*  in -   /Users/700005171/lab/latf/clJson/test/resources/tablesBase3/js/clJson=exadata_activate.py/part-00000-6b9ef644-a1d2-41c6-9d2f-88eb8a56e0e4.c000.txt
  out - exadata_activate */
  private def getSrcName = udf((path: String) => {
    val base = path.split('/').init.last
    val py = base.split('=').last
    py.split('.').init.last
  })

  private def getSchema(col: Column): Column = {
    val noPrefix = regexp_replace(col, """^exadata_""", "")
    val noSuffix = regexp_replace(noPrefix, """_tables$""", "")
    regexp_replace(noSuffix, """_tables_v2$""", "")
  }

  private def flattenTables(df: DataFrame) = {
    val clWithSchema = df.withColumn("path", getSrcName($"fname")).withColumn("pschema", getSchema($"path"))
    val flatBase = clWithSchema.select( $"pschema", $"table_name", $"table_size",
      explode_outer($"schema").as("carr")
    ).select($"pschema".as("schema"), $"table_name", $"table_size",
      $"carr.column_name".as("column_name"),
      $"carr.classification_override".as("classification_override"),
      $"carr.classification_override_reason_code".as("classification_override_reason_code"),
    )

//    clWithSchema.select($"path", $"pschema").groupBy("path", "pschema").count().show(40, false)
    //    flatBase.printSchema()
    //    flatBase.show(false)
    println(s"flatBase.count() = ${flatBase.count()}")
    flatBase
  }


  private def combineOverrides(updateRes: DataFrame, insertRes: DataFrame) = {
    val unionRes = updateRes.union(insertRes).orderBy("schema", "table_name", "column_name")
    val collapseRes = unionRes.groupBy("schema", "table_name", "table_size").agg(collect_list(
      struct(
        $"column_name",
        $"classification_override",
        $"classification_override_reason_code")).as("sch")
    ).select($"schema", $"table_name", $"table_size", filter($"sch", (col) => col.getField("column_name").isNotNull).as("sch"))

//    collapseRes.printSchema()
//    collapseRes.show()
    println(s"collapseRes.count() = ${collapseRes.count()}")
    collapseRes
  }
}

/*=>converter.py
import json
import sys
import os

for file in os.listdir("./rawj"):
    if file.endswith(".json"):
        fp = os.path.join("./rawj", file)
        print(fp)
        with open(fp, 'r') as json_file:
            json_list = list(json_file)
            json_list.sort()

        original_stdout = sys.stdout # Save a reference to the original standard output

        dp = os.path.splitext(file)[0]
        with open(os.path.join(".", f"{dp}.py"), 'w') as f:
            sys.stdout = f # Change the standard output to the file we created.

            print("TABLES = [")
            for json_str in json_list:
                json_str = json_str.replace(',"schema":[]', "")
                result = json.loads(json_str)
                print(f"\t{result},")
            print("]")

            sys.stdout = original_stdout # Reset the standard output to its original value

 */