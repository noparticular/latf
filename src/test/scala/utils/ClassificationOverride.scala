package utils

import au.com.latitudefinancial.utils.SparkContextContainer
import org.apache.spark.sql.{Column, SaveMode}
import org.apache.spark.sql.functions._
import org.scalatest.FunSuite

class ClassificationOverride extends FunSuite with SparkContextContainer {
  import spark.implicits._


  test("merge nested json meta-data") {
    val mnt = "src/test/resources"


    val tablesList = spark.read.option("multiLine", true).json(s"$mnt/tables.json")
    val baseOverridesList = spark.read.option("multiLine", true).json(s"$mnt/table_overrides.json")
    val newOverridesCsv = spark.read.option("header", true).csv(s"$mnt/overrides.csv")

    val newOverrides = newOverridesCsv
      .withColumn("table_name", lower(col("Table_Name")))
      .withColumn("column_name", lower(col("Field_Name")))
      .select("table_name",  "column_name")

    val join = tablesList.join(newOverrides, Seq("table_name"), "left")
    val res = join.groupBy("table_name").agg(collect_list(
      struct(
        $"column_name",
        lit("NONE").as("classification_override"),
        lit("DO_NOT_HASH").as("classification_override_reason_code"))
    ).as("schema"))
//    res.select("table_name", "schema").coalesce(1).write.mode(SaveMode.Overwrite).json(s"$mnt/res.json")

    val bjoin = baseOverridesList.join(newOverrides, Seq("table_name"), "left")
    val gbts = bjoin.groupBy("table_name","schema").agg(collect_list("column_name").as("column_names"))
    val trf= gbts.withColumn("new_schema", transform(col("column_names"),
      (col: Column) => struct(
        col.as("column_name"),
        lit("NONE").as("classification_override"),
        lit("DO_NOT_HASH").as("classification_override_reason_code")
      )
    )).withColumn("original", transform($"schema.column_name", c => lower(c)))
      .withColumn("schema", transform(col("schema"),
        (col: Column) => struct(
          col.getField("column_name").as("column_name"),
          col.getField("classification_override").as("classification_override"),
          col.getField("classification_override_reason_code").as("classification_override_reason_code"))
      ))

    val common_fields = trf.withColumn("common", array_intersect($"original", $"column_names"))
    val filtered = common_fields.withColumn("filtered_schema",
      filter($"schema",
        overriden_field => not(array_contains($"common",
          lower(overriden_field.getField("column_name")))
        )
      )
    )

    filtered.select($"table_name", $"common", $"column_names" as "asked", $"original", $"filtered_schema.column_name" as "filtered").where((size($"common") > 0)).show(50, false)
//    dm_acct_details.LEGALADDRESS_COUNTRY
    val res2 = filtered.withColumn("merged_schema", array_union($"filtered_schema", $"new_schema"))
    res2.select($"table_name", $"merged_schema" as "schema").coalesce(1).write.mode(SaveMode.Overwrite).json(s"$mnt/res2.json")


  }
}
