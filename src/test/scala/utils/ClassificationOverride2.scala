package utils

import au.com.latitudefinancial.utils.SparkContextContainer
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, SaveMode}
import org.scalatest.FunSuite

class ClassificationOverride2 extends FunSuite with SparkContextContainer {
  import spark.implicits._


  test("merge nested json meta-data") {
    val mnt = "src/test/resources"


    val tablesList = spark.read.option("multiLine", true).json(s"$mnt/tables.json")
    val newOverridesCsv = spark.read.option("header", true).csv(s"$mnt/repuser_pvt_result2-2 reviewed.csv")

    val newOverrides = newOverridesCsv
      .withColumn("table_name", lower(col("TABLE_NAME")))
      .withColumn("column_name", upper(col("COLUMN_NAME")))
      .withColumn("classification_override", upper(col("CLASSIFICATION_OVERRIDE")))
      .withColumn("classification_override_reason_code", upper(col("CLASSIFICATION_OVERRIDE_REASON_CODE")))
      .select("table_name",  "column_name", "classification_override", "classification_override_reason_code")

    val join = tablesList.join(newOverrides, Seq("table_name"), "left")
    val res = join.groupBy("table_name").agg(collect_list(
      struct(
        $"column_name",
        $"classification_override",
        $"classification_override_reason_code"
        )
    ).as("schema"))
    res.select("table_name", "schema").coalesce(1).write.mode(SaveMode.Overwrite).json(s"$mnt/res3.json")
  }
}
