package utils

import au.com.latitudefinancial.utils.SparkContextContainer
import com.amazonaws.auth.profile.ProfilesConfigFile
import org.apache.spark.SparkContext
import org.scalatest.FunSuite

class TealiumCompromised extends FunSuite with SparkContextContainer {

  val lcl = "src/test/resources"
//  val mnt = "s3a://lfscnpcom-datalake2-prod-raw/tealium"
  val mnt = "s3a://lfsc360-prod-tealium-eventstream-raw/ingest_tealium_eventstream/tealium_visitor"

  val sc: SparkContext = spark.sparkContext

  System.setProperty(ProfilesConfigFile.AWS_PROFILE_SYSTEM_PROPERTY, "prglass")
  spark.sparkContext.hadoopConfiguration.set("fs.s3a.aws.credentials.provider", "com.amazonaws.auth.profile.ProfileCredentialsProvider")

  test("read json"){
    val rpath = s"$mnt/year=2022/month=08/day=26/hour=01/"

//    val tjson = spark.read.json(sc.wholeTextFiles(rpath).flatMap(x => x._2.replaceAll("}\\{", "}#!#{").split("#!#")))
    val cmids = spark.read.csv(s"$lcl/tealium/cmids.csv")
    cmids.createOrReplaceTempView("cmids")

    val tjson = spark.read.json(rpath)
    tjson.createOrReplaceTempView("vstream")

    spark.sql("""select property_sets.`Customer Master IDs List [sos]` from vstream""").show(false)
  /*  val sql1 =
      """ select * from vstream where
        |data.udo.page_pagetype = 'quote-page'
        |AND
        |data.udo.is_production = true
        |AND
        |data.udo.logical_event_trigger = 'contact-details-added'
        |""".stripMargin
    val sql2 =
      """ select * from vstream where
        |data.udo.page_pagetype = 'quote-page'
        |AND
        |data.udo.is_production = true
        |AND
        |data.udo.logical_event_trigger = 'quote-submit'
        |""".stripMargin*/
//    val res1 = spark.sql(sql1).limit(1)
//    println(res1.count())
//    val res2 = spark.sql(sql2).limit(1)

//    res1.write.json(s"$lcl/out1.json")
//    res2.write.json(s"$lcl/out2.json")

    println("done")
  }


  test("read with transform"){
    import spark.implicits._
    val preText = sc.wholeTextFiles(s"$lcl/tealium/*.json").flatMap(x => x._2.replaceAll("}\\{", "}#!#{").split("#!#")).toDS()
    val src = spark.read.json(preText)
    println(src.count())
  }

}