import json
import sys
import os

for file in os.listdir("./rawj"):
    if file.endswith(".json"):
        fp = os.path.join("./rawj", file)
        print(fp)
        with open(fp, 'r') as json_file:
            json_list = list(json_file)
            json_list.sort()

        original_stdout = sys.stdout # Save a reference to the original standard output

        dp = os.path.splitext(file)[0]
        with open(os.path.join(".", f"{dp}.py"), 'w') as f:
            sys.stdout = f # Change the standard output to the file we created.

            print("TABLES = [")
            for json_str in json_list:
                json_str = json_str.replace(',"schema":[]', "")
                result = json.loads(json_str)
                print(f"\t{result},")
            print("]")

            sys.stdout = original_stdout # Reset the standard output to its original value