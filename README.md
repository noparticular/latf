## Coding Challenge - Problem 2

### Task details have been moved to CodingChallengeSpec/README.md

### Constraints
- Language - Scala
- Tools&Libraries - Vanilla Spark 2.4

### How to run
 ```sbt compile test```
 will generate csv data inside src/test/resources
 
 ```sbt run```
will use generated data and produce anonymized output to src/main/resources 

### Out of Scope
- Spark 3.0
- cluster config
- bouncycastle encryption

### Classes of interest
LocalGeneratorRunner.scala - csv file generator, change numberOfRows to 12345678L to go into Gb range

DataGeneration.scala - data generation functions

DataEncryption.scala - data encryption functions

EtlApp.scala - data anonymizing logic 

EtlMain.scala - entry point for cluster deployment
