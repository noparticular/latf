import sbt._

object Dependencies {
  private val providedAssembly = "provided"

  val sparkVersion = "3.0.1"

  val sparkCore = "org.apache.spark" %% "spark-core" % sparkVersion
  val sparkSQL = "org.apache.spark" %% "spark-sql" % sparkVersion
  val sparkHive = "org.apache.spark" %% "spark-hive" % sparkVersion
  val sparkThrift = "org.apache.spark" %% "spark-hive-thriftserver" % sparkVersion


  val spark = Seq(sparkCore, sparkSQL, sparkHive, sparkThrift)


  //org.apache.hadoop:hadoop-aws:2.7.1
  //org.apache.httpcomponents:httpclient:4.3.6
  //org.apache.httpcomponents:httpcore:4.3.3
  //com.amazonaws:aws-java-sdk-core:1.10.27
  //com.amazonaws:aws-java-sdk-s3:1.10.27
  //com.amazonaws:aws-java-sdk-sts:1.10.27
  val snowflakeSparkConnector = "net.snowflake" %% "spark-snowflake" % "2.8.4-spark_3.0"
  val snowflakeJdbc = "net.snowflake" % "snowflake-jdbc" % "3.13.25"
  val httpclient = "org.apache.httpcomponents" % "httpclient" % "4.3.6"
  val httpcore = "org.apache.httpcomponents" % "httpcore" % "4.3.3"
  val scalikeJdbc = "org.scalikejdbc" %% "scalikejdbc" % "4.0.0"
  val logback = "ch.qos.logback" % "logback-classic" % "1.4.5"
  val snowflake = Seq(snowflakeJdbc, snowflakeSparkConnector, httpclient, httpcore, scalikeJdbc, logback)

  val hadoopCommon = "org.apache.hadoop" % "hadoop-common" % "3.3.0"
  val awsHadoop = "org.apache.hadoop" % "hadoop-aws" % "3.3.0"
  val awsSdk = "com.amazonaws" % "aws-java-sdk" % "1.11.908"

  val aws = Seq(awsSdk, awsHadoop, hadoopCommon)

  val ammonite = "com.lihaoyi" % "ammonite" % "2.5.2" cross CrossVersion.full

  val AkkaVersion = "2.6.14"
  val AkkaHttpVersion = "10.1.11"
  val AlpakkaKinesis = "com.lightbend.akka" %% "akka-stream-alpakka-kinesis" % "3.0.4"
  val AkkaStream = "com.typesafe.akka" %% "akka-stream" % AkkaVersion
  val AkkaHttp = "com.typesafe.akka" %% "akka-http" % AkkaHttpVersion

  val streaming = Seq(AlpakkaKinesis, AkkaStream, AkkaHttp)

  val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5" % "test"
  val sttpClient = "com.softwaremill.sttp.client3" %% "core" % "3.8.5"

}
